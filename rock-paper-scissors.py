#!/usr/bin/env python3
"""
A simple game of Rock, Paper, Scissors that is played in a Terminal or
Command Prompt window.

Instructions:
1. Navigate to the directory that contains this script
2. Run the following command:
        python3 rock-paper-scissors.py
3. Follow the prompts on the screen to play the game

(c) 2021 Matthew Gove Web Development, LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import random


# Initialize variables and constants
ROCK = "r"
PAPER = "p"
SCISSORS = "s"
user_is_victorious = True

while user_is_victorious:
    # User's Turn
    user_input = ""
    acceptable_inputs = [ROCK, PAPER, SCISSORS]
    user_prompt = "Make Your Move. Enter 'R' for Rock, 'P' for Paper, or 'S' for Scissors: "

    # Prompt the user for rock/paper/scissors
    while user_input not in acceptable_inputs:
        user_input = input(user_prompt).lower()


    # Computer's Turn
    rps_map = {
        1: ROCK,
        2: PAPER,
        3: SCISSORS,
    }

    # Select a Random Number Between 1 and 3
    computer_number = random.randint(1, 3)

    # Map the Number to Rock, Paper, or Scissors and Record the Computer's Move
    computer_input = rps_map[computer_number].lower()


    # Determine Which Player Won
    result_msg = ""
    msg_map = {
        ROCK: "Rock",
        PAPER: "Paper",
        SCISSORS: "Scissors",
    }

    # Scenario: Tie Game
    if user_input == computer_input:
        result_msg = "We both chose {}. It's a tie!".format(msg_map[user_input])
    
    elif user_input == ROCK:
        # Paper > Rock -- Computer Wins
        if computer_input == PAPER:
            result_msg = "Paper covers Rock. I win!"
            user_is_victorious = False
        
        # Rock > Scissors -- User Wins
        elif computer_input == SCISSORS:
            result_msg = "Rock crushes Scissors. You win!"
    
    elif user_input == PAPER:
        # Paper > Rock -- User Wins
        if computer_input == ROCK:
            result_msg = "Paper covers Rock. You win!"
        
        # Scissors > Paper -- Computer Wins
        elif computer_input == SCISSORS:
            result_msg = "Scissors cuts Paper. I win!"
            user_is_victorious = False
    
    elif user_input == SCISSORS:
        # Scissors > Paper -- User Wins
        if computer_input == PAPER:
            result_msg = "Scissors cuts Paper. You win!"

        # Rock > Scissors -- Computer Wins
        elif computer_input == ROCK:
            result_msg = "Rock crushes Scissors. I win!"
            user_is_victorious = False


    # Print Game Result to Terminal Window
    print(result_msg)
    print("")